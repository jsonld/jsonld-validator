# jsonld-validator

This project is a bare bones JSON-LD validator for Java. It is built on the
[JSON-LD implementation for Java GitHub Project](https://github.com/jsonld-java/jsonld-java).

Specifically, it uses this library to convert a JSON-LD input into a compact 
Java representation where each field is defined as a fully qualified URI. It
then validates each field by checking that the URL exists and retrieving the
schema associated with the URL. The value associated with the field is then
validated against the schema.

Key features include:

*  Extensible type checking system
*  Support for caching of schemas
*  Stand-alone implementation
*  [Spring-Boot based deployment](https://gitlab.com/jsonld/jsonld-validator-service)

## Using the Library

To use this library add the following maven repository:

```
<repository>
  <id>jsonld-repo</id>
  <url>https://gitlab.com/jsonld/mvn-repo/raw/master</url>
</repository>
```

Then add the following dependency:

```
<dependency>
  <groupId>com.gitlab.jsonld</groupId>
  <artifactId>jsonid-validator</artifactId>
  <version>1.0</version>
</dependency>
```

## Example code

```
public class Example {
    private static JsonLdValidator validator = new JsonLdValidator();
    
    public static void main(String[] args) throws Exception {
        Map context = new HashMap();
        JsonLdOptions options = new JsonLdOptions();
        
        InputStream inputStream = new FileInputStream("input.json");
        Object jsonObject = JsonUtils.fromInputStream(inputStream);
        Map<String, Object> compact = JsonLdProcessor.compact(jsonObject, context, options);
        if (validator.validate(compact)) {
            System.out.println(JsonUtils.toPrettyString(compact));
        }
    }
}
```
