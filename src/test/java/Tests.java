import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;

import org.junit.Test;

import jsonld.InvalidJsonLdException;
import jsonld.JsonLdValidator;

public class Tests {
    @SuppressWarnings("rawtypes")
    Map context = new HashMap();
    JsonLdOptions options = new JsonLdOptions();
    JsonLdValidator validator = new JsonLdValidator();

    @Test
    public void validJsonLdTest() throws IOException {
        InputStream inputStream = new FileInputStream("input.json");
        Object jsonObject = JsonUtils.fromInputStream(inputStream);
        Map<String, Object> compact = JsonLdProcessor.compact(jsonObject, context, options);
        if (validator.validate(compact)) {
            System.out.println(JsonUtils.toPrettyString(compact));
        }
    }

    @Test(expected = InvalidJsonLdException.class)
    public void invalidFieldJsonLdTest() throws IOException {
        InputStream inputStream = new FileInputStream("input-badfield.json");
        Object jsonObject = JsonUtils.fromInputStream(inputStream);
        Map<String, Object> compact = JsonLdProcessor.compact(jsonObject, context, options);
        if (validator.validate(compact)) {
            System.out.println(JsonUtils.toPrettyString(compact));
        }
    }

    @Test
    public void kamilJsonLdTest() throws IOException {
        InputStream inputStream = new FileInputStream("jsonld-example.json");
        Object jsonObject = JsonUtils.fromInputStream(inputStream);
        Map<String, Object> compact = JsonLdProcessor.compact(jsonObject, context, options);
        if (validator.validate(compact)) {
            System.out.println(JsonUtils.toPrettyString(compact));
        }
    }
}