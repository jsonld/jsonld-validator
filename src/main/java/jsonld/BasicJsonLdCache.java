package jsonld;

import java.util.Map;
import java.util.TreeMap;

public class BasicJsonLdCache implements JsonLdCache {
    private Map<String, JsonLdSchema> cache = new TreeMap<String, JsonLdSchema>();
    
    public JsonLdSchema retrieve(String url) {
        return cache.get(url);
    }

    @Override
    public void store(String url, JsonLdSchema schema) {
        cache.put(url, schema);
    }

    public String toString() {
        return cache.keySet().toString();
    }
}