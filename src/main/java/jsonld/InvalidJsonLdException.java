package jsonld;

public class InvalidJsonLdException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 2828252785911446873L;
    public InvalidJsonLdException(String msg) {
        super(msg);
    }
	public InvalidJsonLdException(String msg, Throwable th) {
        super(msg, th);
	}
}
