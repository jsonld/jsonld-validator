package jsonld;

public class NullJsonLdCache implements JsonLdCache {
    public JsonLdSchema retrieve(String url) {
        return null;
    }

    @Override
    public void store(String url, JsonLdSchema schema) {
    }
}