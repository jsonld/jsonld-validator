package jsonld;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class JsonLdValidator {
    @SuppressWarnings("rawtypes")
    private static Map context = new HashMap();
    private static JsonLdOptions options = new JsonLdOptions();
    public static Map<String, TypeChecker> typeCheckers = new TreeMap<String, TypeChecker>();

    static {
        typeCheckers.put("http://schema.org/Text", new TypeChecker() {
            public boolean validate(Object value) {
                return String.class.isInstance(value);
            }
        });
        typeCheckers.put("http://schema.org/Number", new TypeChecker() {
            public boolean validate(Object value) {
                return Double.class.isInstance(value) || Integer.class.isInstance(value);
            }
        });
        typeCheckers.put("http://schema.org/Integer", new TypeChecker() {
            public boolean validate(Object value) {
                return Integer.class.isInstance(value);
            }
        });
        typeCheckers.put("http://schema.org/URL", new TypeChecker() {
            @SuppressWarnings("unchecked")
            public boolean validate(Object value) {
                if (!Map.class.isInstance(value))
                    return false;
                Object url = ((Map<String, Object>) value).get("@id");
                try {
                    new URL(url.toString());
                } catch (MalformedURLException e) {
                    return false;
                }
                return true;
            }
        });
    }

    private CloseableHttpClient httpClient = HttpClients.createDefault();
    private JsonLdCache cache = new BasicJsonLdCache();

    public void setCache(JsonLdCache cache) {
        this.cache = cache;
    }
    
    public boolean validate(String jsonString) throws JsonParseException, IOException {
        Object jsonObject = JsonUtils.fromString(jsonString);
        return validate(JsonLdProcessor.compact(jsonObject, context, options));
    }

    public boolean validate(Map<String, Object> compact) {
        for (Entry<String, Object> entry : compact.entrySet()) {
            String url = entry.getKey();
            if (url.startsWith("http")) {
                JsonLdSchema schema = null;
                try {
                    schema = getSchema(url);
                } catch (IOException e) {
                    throw new InvalidJsonLdException("Failed to load schema for: " + url, e);
                }

                if (!valid(entry.getValue(), schema.getTypes())) {
                    throw new InvalidJsonLdException("Could not validate field: " + url + " with value: '" + entry.getValue() + "' from types: " + schema.getTypes());
                }
            }
        }
        return true;
    }

    private JsonLdSchema getSchema(String url) throws IOException {
        JsonLdSchema schema = cache.retrieve(url);
        if (schema == null) {
            System.out.println("[INFO] Downloading: " + url+".jsonld");

            HttpGet request = new HttpGet(url+".jsonld");
            CloseableHttpResponse response =  httpClient.execute(request);
            try {
                Object defn = JsonUtils.fromInputStream(response.getEntity().getContent());
                schema = new JsonLdSchema(url, JsonLdProcessor.compact(defn, context, options));
            } catch (UnsupportedOperationException | IOException exception) {
            } finally {
                response.close();
            }

            if (schema == null) {
                throw new InvalidJsonLdException("Json-ld Document contains an invalid field name: " + url);
            }
            cache.store(url, schema);

            // Pre-load parent schemas
            for (String parentUrl : schema.getParentUrls()) {
                getSchema(parentUrl);
            }
        }
        return schema;
    }

    @SuppressWarnings("unchecked")
    public boolean validate(Object value, JsonLdSchema schema) {
        if (Map.class.isInstance(value)) {
            if (schema.is("@type", "http://www.w3.org/2000/01/rdf-schema#Class")) {
                Object id = ((Map<String, Object>) value).get("@id");
                Object type = ((Map<String, Object>) value).get("@type");
                if (id != null) {
                    for (String parentUrl : schema.getParentUrls()) {
                        try {
                            if (validate(id, getSchema(parentUrl))) return true;
                        } catch (IOException e) {
                        }
                        throw new InvalidJsonLdException("Could not resolve parent schemas for: " + schema.getUrl());
                    }
                } else if (type != null) {
                    try {
                        JsonLdSchema schema2 = getSchema(type.toString());
                        if (isSubclass(schema2, schema)) {
                            return validate((Map<String, Object>) value);
                        }
                    } catch (IOException e) {
                        throw new InvalidJsonLdException("Could not resolve subtype: " + type, e);
                    }
                }
            }
        } else if (ArrayList.class.isInstance(value)) {
            for (Object element : (ArrayList<Object>) value) {
                if (!validate(element, schema)) return false;
            }
            return true;
        }
        TypeChecker typeChecker = typeCheckers.get(schema.getUrl());
        if (typeChecker == null) return false;

        return typeChecker.validate(value);
    }

    private boolean isSubclass(JsonLdSchema child, JsonLdSchema target) throws IOException {
        if (child.getUrl().equals(target.getUrl())) return true;
        for (String parentUrl : child.getParentUrls()) {
            JsonLdSchema parent = getSchema(parentUrl);
            if (isSubclass(parent, target)) return true;
        }
        return false;
    }

    private boolean valid(Object value, List<String> types) {
        for (String type : types) {
            try {
                if (validate(value, getSchema(type))) return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
                    
        }
        return false;
    }
}