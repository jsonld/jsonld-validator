package jsonld;

public interface JsonLdCache {
    JsonLdSchema retrieve(String url);
    void store(String url, JsonLdSchema schema);
}