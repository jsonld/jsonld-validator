package jsonld;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import com.github.jsonldjava.utils.JsonUtils;

public class JsonLdSchema {
    private String url;
    private Map<String, Object> schema;
    private Map<String, Object> definition;
    private Set<String> parentUrls = new TreeSet<String>();
    private List<String> types = new ArrayList<String>();

    public JsonLdSchema(String url, Map<String, Object> schema) {
        this.url = url;
        this.schema = schema;
        this.definition = schema;

        // Extract core definition from RDF graph
        extractDefinition();
        this.types = getValidTypes();
        if (is("@type", "http://www.w3.org/2000/01/rdf-schema#Class")) {
            extractParents();
        }
    }

    @SuppressWarnings("unchecked")
    private void extractParents() {
        Object subclass = definition.get("http://www.w3.org/2000/01/rdf-schema#subClassOf");
        if (ArrayList.class.isInstance(subclass)) {
            System.out.println("[JsonLdSchema] List of parents Not handled yet");
            System.exit(0);
        } else if (Map.class.isInstance(subclass)) {
            String url = ((Map<String, Object>) subclass).get("@id").toString();
            parentUrls.add(url);
        }
    }

    @SuppressWarnings("unchecked")
    private void extractDefinition() {
        Object graph = schema.get("@graph");
        if (graph != null) {
            for (Map<String, Object> element : (ArrayList<Map<String, Object>>) graph) {
                if (this.url.equals(element.get("@id"))) {
                    definition = element;
                    return;
                }
            }
        }
    }

    private List<String> getValidTypes() {
        return extractTypes(definition.get("http://schema.org/rangeIncludes"), new ArrayList<String>());
    }

    @SuppressWarnings("unchecked")
    private List<String> extractTypes(Object object, List<String> types) {
        if (ArrayList.class.isInstance(object)) {
            for (Object o : (ArrayList<?>) object) {
                extractTypes(o, types);
            }
        } else if (Map.class.isInstance(object)) {
            for (Entry<String, String> entry : ((Map<String, String>) object).entrySet()) {
                if (entry.getKey().equals("@id"))
                    types.add(entry.getValue());
            }
        }
        return types;
    }

    public String toString() {
        try {
            return JsonUtils.toPrettyString(schema);
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    @SuppressWarnings("rawtypes")
	public boolean is(String field, String target) {
        Object value = definition.get(field);
        if (ArrayList.class.isInstance(value)) {
            for (Object element : (ArrayList) value) {
                if (element.equals(target)) return true;
            }
            return false;
        }
		return value.equals(target);
	}

	public Object getUrl() {
		return url;
	}

	public Object get(String url) {
		return definition.get(url);
    }
    
    public Set<String> getParentUrls() {
        return parentUrls;
    }

    public List<String> getTypes() {
        return types;
    }
}