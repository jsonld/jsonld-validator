package jsonld;

public interface TypeChecker {
    boolean validate(Object value);
}