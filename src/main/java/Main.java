import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;

import jsonld.BasicJsonLdCache;
import jsonld.JsonLdValidator;

public class Main {
    @SuppressWarnings("rawtypes")
    public static void main(String[] args) throws Exception {
        Map context = new HashMap();
        JsonLdOptions options = new JsonLdOptions();
        JsonLdValidator validator = new JsonLdValidator();
        validator.setCache(new BasicJsonLdCache());

        // InputStream inputStream = new FileInputStream("input.json");
        InputStream inputStream = new FileInputStream("jsonld-example.json");
        Object jsonObject = JsonUtils.fromInputStream(inputStream);
        Map<String, Object> compact = JsonLdProcessor.compact(jsonObject, context, options);
        System.out.println(JsonUtils.toPrettyString(compact));
        if (validator.validate(compact)) {
            System.out.println(JsonUtils.toPrettyString(compact));
        }
    }
}